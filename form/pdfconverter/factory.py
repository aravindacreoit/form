import factory
from faker import Faker
import random

from .models import Order

fake = Faker()


class OrderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Order

    code = factory.Faker('bban')
    description = factory.Faker('paragraph')
    discount = 0.0

    @factory.lazy_attribute
    def quantity(self):
        return random.randint(10, 100)

    @factory.lazy_attribute
    def price_per_unit(self):
        return random.uniform(10.0, 100.0)

    @factory.lazy_attribute
    def discount(self):
        return random.uniform(10.0, 30.0)

    @factory.lazy_attribute
    def date(self):
     return fake.date_time_between(start_date="-5y", end_date="now", tzinfo=None)


def create_orders(size=1):
    OrderFactory.create_batch(size)

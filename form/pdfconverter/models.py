from django.db import models


class Order(models.Model):
    code = models.CharField(max_length=255)
    description = models.TextField()
    quantity = models.IntegerField(default=0)
    price_per_unit = models.DecimalField(default=0.0, max_digits=10, decimal_places=2)
    discount = models.DecimalField(default=0.0, max_digits=10, decimal_places=2)
    date = models.DateField(null=True)

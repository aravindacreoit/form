from django.template.loader import render_to_string
from django.http import HttpResponse
import weasyprint
import datetime
from .models import Order
from faker import Faker
from django.db.models import Sum, F, ExpressionWrapper, DecimalField
from django.conf import settings
from django.shortcuts import render


def get_orders(request, year):
    fake = Faker()
    orders = Order.objects.filter(date__year=year).annotate(
        total_price=ExpressionWrapper(F('price_per_unit')*F('quantity')-F('discount'), output_field=DecimalField(
            max_digits=10, decimal_places=2)))
    total = orders.aggregate(total_payable=Sum('total_price', output_field=DecimalField()))
    context = {
                            'orders': orders,
                            'year': year,
                            'company_name':fake.company(),
                            'address': fake.simple_profile(sex=None)['address'],
                            'phone': fake.phone_number(),
                            'fax':'YEGYE898',
                            'billing_address':fake.simple_profile(sex=None)['address'],
                            'delivery_address': fake.simple_profile(sex=None)['address'],
                            'suppier_account': fake.bban(),
                            'purchase_executive': fake.job(),
                            'currency': fake.currency_code(),
                            'order_no': fake.isbn13(separator="-"),
                            'date': datetime.datetime.today(),
                            'us_dollar': fake.isbn13(separator="-"),
                            'total_payable': total['total_payable'],
                            'payment': 'T/T',
                             }
    html_string = render_to_string('pdfconverter/purchase_order.html', context)
    response = HttpResponse(f"Ordes file orders_{year} saved successfully")
    html = weasyprint.HTML(string=html_string)
    html.write_pdf(target=f'/home/aravinda/Downloads/orders_{year}_{datetime.datetime.today()}',
                   stylesheets=[weasyprint.CSS(settings.BASE_DIR+'/pdfconverter/static/pdfconverter/style.css')]
                   )
    return response

from . import views
from django.urls import path

urlpatterns = [
    path('order/<int:year>/', views.get_orders, name='get_orders'),
]